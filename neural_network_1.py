# helper to load data from PNG image files
import imageio
# glob helps select multiple files using patterns
import glob
import numpy
# библиотека scipy.special содержит сигмоиду expit()
import scipy.special
#  библиотека для графического отображения массивов
import matplotlib.pyplot


# определение класса нейронной сети
class NeuralNetwork:
    #  инициализировать нейронную сеть
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate):
        # задать количество узлов во входном, скрытом и выходном слое
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes

        # Матрицы весовых коэффициентов связей wih (между входным и скрытым
        #  слоями) и who (между скрытым и выходным слоями).
        #  Весовые коэффициенты связей между узлом i и узлом j следующего слоя
        #  обозначены как w_i_j: wll w21 # wl2 w22 и т.д.
        # Важно self.hnodes, self.inodes a не self.inodes, self.hnodes
        self.wih = (numpy.random.rand(self.hnodes, self.inodes) - 0.5)
        self.who = (numpy.random.rand(self.onodes, self.hnodes) - 0.5)

        # коэффициент обучения
        self.lr = learningrate

        # использование сигмоиды в качестве функции активации
        self.activation_function = lambda x: scipy.special.expit(x)
        pass

    # тренировка нейронной сети
    def train(self, inputs_list, targets__list):
        # преобразовать списоки входных значений и ответы тренировочных примеров в матрицы
        inputs = numpy.array(inputs_list, ndmin=2).T
        targets = numpy.array(targets__list, ndmin=2).T

        # рассчитать входящие сигналы для скрытого слоя
        hidden_inputs = numpy.dot(self.wih, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)

        # рассчитать входящие сигналы для выходного слоя
        final_inputs = numpy.dot(self.who, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)

        # ошибки выходного слоя = (целевое значение - фактическое значение)
        output_errors = targets - final_outputs
        # ошибки скрытого слоя - это ошибки output_errors,
        # распределенные пропорционально весовым коэффициентам связей
        # и рекомбинированные на скрытых узлах
        hidden_errors = numpy.dot(self.who.T, output_errors)
        # обновить весовые коэффициенты для связей между
        #  скрытым и выходным слоями
        self.who += self.lr * numpy.dot((output_errors * final_outputs * (1.0 - final_outputs)),
                                        numpy.transpose(hidden_outputs))
        # обновить весовые коэффициенты для связей между
        #  входным и скрытым слоями
        self.wih += self.lr * numpy.dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)),
                                        numpy.transpose(inputs))

        pass

    # опрос нейронной сети
    def query(self, inputs_list):
        # преобразовать список входных значений в двухмерный массив
        inputs = numpy.array(inputs_list, ndmin=2).T
        # рассчитать входящие сигналы для скрытого слоя
        hidden_inputs = numpy.dot(self.wih, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)
        # рассчитать входящие сигналы для выходного слоя
        final_inputs = numpy.dot(self.who, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)

        return final_outputs


# количество входных, скрытых и выходных узлов
input_nodes = 784
hidden_nodes = 200
output_nodes = 10
#  коэффициент обучения равен 0,3
learning_rate = 0.3

# создать экземпляр нейронной сети
n = NeuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)
# загрузить в список тестовый набор данных CSV-файла набора MNIST
training_data_file = open("D:/data_mnist/mnist_train.csv", 'r')
training_data_list = training_data_file.readlines()
training_data_file.close()

# тренировка нейронной сети
# epochs это кол-во раз использования тренировочных данных
epochs = 5
# перебрать все записи в тренировочном наборе данных
for e in range(epochs):
    for record in training_data_list:
        # получить список значений, используя символы запятой (1,1)
        #  в качестве разделителей
        all_values = record.split(',')
        #  масштабировать и сместить входные значения
        inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        #  создать целевые выходные значения
        #  (все равны 0,01, за исключением # желаемого маркерного значения, равного 0,99)
        targets = numpy.zeros(output_nodes) + 0.01
        # all_values[0] is the target label
        targets[int(all_values[0])] = 0.99
        n.train(inputs, targets)

# собственные ьренировочные данные
our_own_dataset = []
# загружаем картинку
image_file_name = 'D:/data_for_machine_learning/test_2.png'
# определяем последнее знаение как целевое значение
label = int(image_file_name[-5:-4])

# загружаем тренировочные данные как в массив
print("loading ... ", image_file_name)
img_array = imageio.imread(image_file_name, as_gray=True)

# преобразуем из 28 х 28 в список длиной 784 символа
img_data = 255.0 - img_array.reshape(784)

# масштабируем данные от 0.01 до 1
img_data = (img_data / 255.0 * 0.99) + 0.01
print(numpy.min(img_data))
print(numpy.max(img_data))

# создаем список из целевого сигнала и масштабируемых данных
record = numpy.append(label, img_data)
print(record)
our_own_dataset.append(record)


# тестирование

# record to test
item = 0

# правильный сигнал это первое число
correct_label = our_own_dataset[item][0]
# данные это остаток (без целевого значения)
inputs = our_own_dataset[item][1:]

# опрос
outputs = n.query(inputs)
print(outputs)

label = numpy.argmax(outputs)
print("network says ", label)
if label == correct_label:
    print("match!")
else:
    print("no match!")
    pass
